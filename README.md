# simple-unit-testing

Single-header library for unit-testing in C99.

## Quick start

- Drop the header in your source tree and include it in the files to be tested.
- Declare a test case, using the `SUT_TEST` macro.
- Declare a test suite using `SUT_TEST_SUITE`, `SUT_TEST_SUITE_ADD` and `SUT_TEST_SUITE_END`.
- Create a runner for your test: this C file should contain only `SUT_DECLARE_MAIN_FUNC`.
			    
## Declaring test cases

The tests should be declared as follows, for example at the bottom of
each source file if you want to test the internal implementation of
your code, or in separate files if you only want to test the public
interface.

```
SUT_TEST(unique_name) {
  // Code of this test
}
SUT_TEST(another_cool_test_name) {
  // Code of this test
}
```

##  Writing tests

In each test case, you can use any of the following macro to test various things.
You can first test whether two values are equal:

```
SUT_CHAR_EQUAL(expected, actual, ...)
SUT_INT_EQUAL(expected, actual, ...)
SUT_DOUBLE_EQUAL(expected, actual, ...)
SUT_STR_EQUAL(expected, actual, ...)
```

It is sometimes annoying to find the difference between two strings,
so SUT_STR_DIFF uses `diff` on disk to expose it more clearly.

```
SUT_STR_DIFF(expected, actual, ...)
```

You can also assert things.


```
SUT_ASSERT(assertion, ...)
SUT_ASSERT_TRUE(assertion, ...) // Exactly equivalent to SUT_ASSERT
SUT_ASSERT_FALSE(assertion, ...) // fails if the assertion is true
```

Any of these macro can be used with the minimal amount of parameters, eg:

```
SUT_ASSERT_TRUE(my_func(toto) > 1)
```

Or you can pass additional parameters that will be passed to fprintf(stderr), eg:

```
SUT_ASSERT_TRUE(my_func(toto) > 1, "The value %d leads to a negative value %f", toto, my_func(toto))
```

## Populating a test suite


Then, all tests are grouped within a suite as follows. 
You can have as many suites as you want in your project.

```
SUT_TEST_SUITE(module_name) = {
   SUT_TEST_SUITE_ADD(unique_name),
   SUT_TEST_SUITE_ADD(another_cool_test_name),
   // possibly more tests added
   SUT_TEST_SUITE_END
};
```

That's all what you have to do to register your tests to the runner.

## Getting a runner

You need to have a C file with the following content (only these 3 lines):

```
#include "simple_unit_test.h"

SUT_DECLARE_MAIN_FUNC // Write a main() function for your runner
```

When run, the resulting binary will run all tests one by one and report any errors encountered.
You may want to run your test runner in valgrind for more insight.

## Troubleshooting

If you have any issue with this code, please open an issue.

## TODO

- Write some examples and maybe more documentation.
- The code supports setup and teardown functions, but not the public API
- The runner is minimalistic. Without going for the full fork() game, we could give the suite names
