#include "line.h"
#include "simple_unit_test.h"

struct line {
  point_t* p1;
  point_t* p2;
};

line_t* line_create(point_t* p1, point_t* p2)
{
  line_t* res = malloc(sizeof(line_t));
  res->p1     = p1;
  res->p2     = p2;
  return res;
}
void line_free(line_t* l)
{
  point_free(l->p1);
  point_free(l->p2);
  free(l);
}

void line_move(line_t* l, double dx, double dy)
{
  point_move(l->p1, dx, dy);
  point_move(l->p2, dx, dy);
}

SUT_TEST(line_create)
{
  line_t* l = line_create(point_create(12, 23), point_create(34, 45));
  SUT_DOUBLE_EQUAL(12., point_getX(l->p1), "The x coordinate of p1 is supposed to be 12");
  SUT_DOUBLE_EQUAL(23., point_getY(l->p1), "The y coordinate of p1 is supposed to be 23");
  SUT_DOUBLE_EQUAL(34., point_getX(l->p2), "The x coordinate of p2 is supposed to be 34");
  SUT_DOUBLE_EQUAL(45., point_getY(l->p2), "The y coordinate of p2 is supposed to be 45");
  line_free(l);

  return 1;
}

SUT_TEST(line_move)
{
  line_t* l = line_create(point_create(12, 23), point_create(34, 45));
  line_move(l, 100, 100);
  SUT_DOUBLE_EQUAL(112., point_getX(l->p1), "The x coordinate of p1 is supposed to be 112");
  SUT_DOUBLE_EQUAL(123., point_getY(l->p1), "The y coordinate of p1 is supposed to be 123");
  SUT_DOUBLE_EQUAL(134., point_getX(l->p2), "The x coordinate of p2 is supposed to be 134");
  SUT_DOUBLE_EQUAL(145., point_getY(l->p2), "The y coordinate of p2 is supposed to be 145");
  line_free(l);

  return 1;
}

SUT_TEST_SUITE(line) = {SUT_TEST_SUITE_ADD(line_create), SUT_TEST_SUITE_ADD(line_move), SUT_TEST_SUITE_END};
