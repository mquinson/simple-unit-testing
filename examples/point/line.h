#ifndef LINE_H
#define LINE_H

#include "point.h"

typedef struct line line_t;

line_t* line_create(point_t* p1, point_t* p2);
void line_free(line_t* l);

void line_move(line_t* l1, double dx, double dy);
void line_display(line_t* l);

#endif /* LINE_H */
