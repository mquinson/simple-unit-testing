#include "point.h"
#include "simple_unit_test.h"

#include <stdio.h>
#include <stdlib.h>

int maxRank = 0;
struct point {
  int rank;
  double x, y;
};

point_t* point_create(double x, double y)
{
  point_t* res = malloc(sizeof(point_t));
  res->rank    = maxRank++;
  res->x       = x;
  res->y       = y;
  return res;
}
void point_free(point_t* p)
{
  free(p);
}
double point_getX(point_t* p)
{
  return p->x;
}

double point_getY(point_t* p)
{
  return p->y;
}

void point_move(point_t* p, double dx, double dy)
{
  p->x += dx;
  p->y += dy;
}
point_t* point_add(point_t* p1, point_t* p2)
{
  return point_create(p1->x + p2->x, p1->y + p2->y);
}

void point_display(point_t* p)
{
  printf("%d: %g, %g\n", p->rank, p->x, p->y);
}

SUT_TEST(point_create)
{
  point_t* p = point_create(12, 23);
  SUT_DOUBLE_EQUAL(12., p->x, "The x coordinate is supposed to be 12");
  SUT_DOUBLE_EQUAL(23., p->y, "The y coordinate is supposed to be 23");
  point_free(p);

  return 1;
}
SUT_TEST(point_getters)
{
  point_t* p = point_create(12, 23);
  SUT_DOUBLE_EQUAL(12., point_getX(p), "The x coordinate is supposed to be 12");
  SUT_DOUBLE_EQUAL(23., point_getY(p), "The y coordinate is supposed to be 23");
  point_free(p);

  return 1;
}

SUT_TEST(point_add)
{
  point_t* p1   = point_create(12, 23);
  point_t* p2   = point_create(10, 20);
  point_t* psum = point_add(p1, p2);
  SUT_DOUBLE_EQUAL(22., psum->x, "The x coordinate is supposed to be 22");
  SUT_DOUBLE_EQUAL(43., psum->y, "The y coordinate is supposed to be 43");
  point_free(p1);
  point_free(p2);
  point_free(psum);

  return 1;
}

SUT_TEST_SUITE(point) = {SUT_TEST_SUITE_ADD(point_create), SUT_TEST_SUITE_ADD(point_getters),
                         SUT_TEST_SUITE_ADD(point_add), SUT_TEST_SUITE_END};
