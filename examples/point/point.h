#ifndef POINT_H
#define POINT_H

typedef struct point point_t;

point_t* point_create(double x, double y);
void point_free(point_t* p);

double point_getX(point_t* p);
double point_getY(point_t* p);

void point_move(point_t* p1, double dx, double dy);
point_t* point_add(point_t* p1, point_t* p2);
void point_display(point_t* p);

#endif /* POINT_H */
